import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Route } from "react-router-dom"
import AddAuthor from "./components/AddAuthor"

// apollo client setup
const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <Router>
            <div>
                <Route exact  path="/" component= {App} />    
                <Route path="/AddAuthor" component= {AddAuthor} />
            </div>
        </Router>
</ApolloProvider>, 
    
document.getElementById('root')

)